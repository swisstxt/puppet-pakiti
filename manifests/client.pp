class pakiti::client {
  package{'pakiti-client':
    ensure => present,
  }->
  file{'/etc/pakiti/pakiti2-client.conf':
    content => template('pakiti/pakiti2-client.conf'),
    owner   => root,
    group   => root,
    mode    => '0444',
  }
}
