class pakiti::server(
  $dbserver = 'localhost',
  $dbname = 'pakiti',
  $dbusername = 'pakiti',
  $dbpassword,
) {
  Class['pakiti::server'] <- Class['mysql::server']

  include ::apache
  include ::php
  include ::php::extensions::mysql
  include ::php::extensions::xml

  mysql::db{$dbname:
    user => $dbuser,
    password => $dbpassword,
    host => $dbserver,
    grant => ['all'],
  }->
  package{'pakiti-server':
    ensure => present,
  }->
  file{'/etc/pakiti/pakiti2-server.conf':
    content => template('pakiti/pakiti2-server.conf'),
    owner   => root,
    group   => root,
    mode    => '0444',
  }~>
  service{'pakiti2':
    ensure    => running,
    enable    => true,
    hasstatus => true,
  }
  file{'/etc/httpd/conf.d/paktit2.conf':
    content => template('pakiti/apache-vhost.conf'),
    owner   => root,
    group   => root,
    mode    => '0444',
    notify  => Class['apache::service'],
  }
}
